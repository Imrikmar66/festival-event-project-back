<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Collection;
use App\Style;
use App\Photo;
use App\Social;
use App\Organization;

class StoreArtistRequest extends FormRequest
{

    protected function failedValidation(Validator $validator) {
        // dd( $validator->failed() );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'styles' => 'required|array',
            'styles.*' => [ 'required', Rule::in(Style::all()->pluck('id')) ],
            'pictures' => 'required|array',
            'pictures.*' => 'required',
            'organization' => 'required'
        ];
    }

    public function getOrganization(): Organization {
        $validated = collect($this->validated());
        return Organization::find( $validated->get('organization') );
    }

    public function getPhotos(): Collection {

        $validated = collect($this->validated());
        $form_photos = $validated->only('pictures')->all();
        $photos = collect($form_photos['pictures'])->map(function ($item, $key) {
            $photo = new Photo;
            $photo->url = $item;
            return $photo;
        });
        return $photos;

    }

    public function getStyles(): Collection {

        $validated = collect($this->validated());
        $form_styles = $validated->only('styles')->all();
        $styles = collect($form_styles['styles'])->map(function ($item, $key) {
            return Style::find( $item );
        });
        return $styles;

    }

    public function getSocials(): Collection {

        $form_social_labels = $this->get('social_labels');
        $form_social_urls = $this->get('social_urls');

        $socials = new Collection([]);
        foreach( $form_social_labels as $key => $label ) {

            $form_social_icon = $this->get('icon_' . $key);

            if( empty( $form_social_urls[$key] ) || empty( $form_social_icon ) ) continue;
            
            $social = new Social;
            $social->label = $label;
            $social->url = $form_social_urls[$key];
            $social->icon_id = $form_social_icon;

            $socials->push( $social );
        }
        return $socials;

    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        // Convert empty arrays with [0]=>null in request to []
        $this->convertEmptyArrays();

        $validator->after(function ($validator) {
            if ( !$this->checkSocials() ) {
                $validator->errors()->add('socials', 'Socials fields are not correct');
            }
        });
    }

    private function checkSocials() {

        // Check if almost one array is fill
        if( 
            $this->almostOneIsFill( [ 'social_labels', 'social_urls', 'icons' ] ) 
        ) {
            
            // If this field we loop is empty, cancel
            if( !$this->social_labels ) return false;

            foreach ( $this->social_labels as $key => $social ) {
                
                if( 
                    empty($this->social_labels[$key]) 
                    || empty($this->social_urls[$key]) 
                    || empty($this->icons[$key] ) 
                ) return false;

            }

            return true;

        }
        
        return true;

    }

    private function convertEmptyArrays() {
        foreach( $this->request->all() as $key => $bag_element ) {
            if( is_array( $bag_element ) && empty( $bag_element[0] ) ) {
                $this->$key = [];
            }
        }
    }

    private function almostOneIsFill( array $fields ) {

        foreach ($fields as $field) {
            
            if( !empty( $this->$field )  )  return true;

        }

        return false;

    }

}
