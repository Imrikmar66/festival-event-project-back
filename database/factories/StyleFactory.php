<?php

use Faker\Generator as Faker;

$pointer = 0;
$factory->define(App\Style::class, function (Faker $faker) use (&$pointer) {
    $styles = new  \Illuminate\Support\Collection([
        'hardstyle',
        'hardstyle-eupho',
        'rawstyle',
        'xtra-raw',
        'hardcore',
        'frenchcore',
        'uptempo',
        'terrorcore'
    ]);
    $label = $styles->get($pointer);
    $pointer++;
    return [
        'label' => $label
    ];
});
