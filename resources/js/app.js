
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });

const $DOM = {
    buttons: {
        picture: document.getElementById('add-picture'),
        social: document.getElementById('add-social')
    },
    container: {
        picture: document.getElementById('form-pictures'),
        social: document.getElementById('form-socials')
    },
    clone: {
        picture: document.getElementsByClassName('pictures-container')[0],
        socials: document.getElementsByClassName('socials-container'),
        social: document.getElementsByClassName('socials-container')[0]
    }
};

if( $DOM.buttons.picture && $DOM.buttons.social ) {

    $DOM.buttons.picture.addEventListener('click', e => {
        e.preventDefault();
        const $newNode = $DOM.clone.picture.cloneNode(true);
        $DOM.container.picture.append($newNode);
        $DOM.container.picture.append($DOM.buttons.picture);
    });

    $DOM.buttons.social.addEventListener('click', e => {
        e.preventDefault();

        const $newNode = $DOM.clone.social.cloneNode(true);
        $inputs = $newNode.querySelectorAll('.form-check-input');
        [...$inputs].map( ( $input ) => {
            $input.name = `icon_${$DOM.clone.socials.length}`;
            return $input;
        } );
        
        $DOM.container.social.append($newNode);
        $DOM.container.social.append($DOM.buttons.social);
    });

}