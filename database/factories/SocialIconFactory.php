<?php

use Faker\Generator as Faker;

$icon_pointer = 0;
$factory->define(App\SocialIcon::class, function (Faker $faker) use (&$icon_pointer) {
    $rs = new  \Illuminate\Support\Collection([
        'twitter',
        'facebook',
        'soundclound',
        'spotify',
        'youtube'
    ]);
    $label = $rs->get($icon_pointer);
    $icon_pointer++;

    return [
        'label' => $label,
        'url' => $faker->url
    ];
});
