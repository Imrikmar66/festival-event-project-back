<?php

use Faker\Generator as Faker;

$factory->define(App\Timetable::class, function (Faker $faker, $args) {
    // $start = $faker->dateTimeBetween('now', '+24 hours');
    // $end = $faker->dateTimeBetween( $start->format('Y-m-d H:i:s'), $start );
    return [
        'artist_id' => $args['artist_id'],
        'room_id' => $args['room_id'],
        'start' => $faker->dateTime(),
        'end' => $faker->dateTime()
    ];
});
