<div class="card" style="width: 18rem;">

    @if( $artist->info->photos->isNotEmpty() )
        <div 
            class="custom-card-img-bg" 
            style="background-image:url({{ Storage::url( $artist->info->photos->first()->url ) }})">
        </div>
    @endif

    <div class="card-body">
        <h5 class="card-title"> {{ $artist->info->name }} </h5>
        <p class="card-text"> {{ $artist->info->description }} </p>
        <a href="{{ route('artists.show', $artist->id) }}" class="btn btn-primary"> See artist </a>
    </div>

</div>