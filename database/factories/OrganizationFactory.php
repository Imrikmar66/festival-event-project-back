<?php

use Faker\Generator as Faker;

$factory->define(App\Organization::class, function (Faker $faker) {
    $info_id = factory(App\Info::class)->create()->id;
    return [
        'info_id' => $info_id
    ];
});
