@extends('layouts.app')

@section('content')
    
    <ul>
        @foreach ($organizations as $organization)

            <li> {{ $organization }} </li>

        @endforeach
    </ul>

@endsection