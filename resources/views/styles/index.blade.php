@extends('layouts.app')

@section('content')
    
    <ul>
        @foreach ($styles as $style)

            <li> {{ $style }} </li>

        @endforeach
    </ul>

@endsection