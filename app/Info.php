<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Info extends Model
{

    protected $fillable = ['name', 'description'];
    
    public function socials() {
        return $this->hasMany( Social::class );
    }

    public function photos() {
        return $this->hasMany( Photo::class );
    }

    public function organization() {
        return $this->hasOne( Organization::class );
    }

    public function saveOrganization( Organization $organization ) {
        $this->organization()->save( $organization );
    }

    public function artist() {
        return $this->hasOne( Artist::class );
    }

    public function saveArtist( Artist $artist ) {
        $this->artist()->save( $artist );
    }

    public function addPhoto( Photo $photo ) {
        $this->photos()->save( $photo );
    }

    public function addPhotos( Collection $photos ) {
        $this->photos()->saveMany( $photos );
    }

    public function addSocial( Social $social ) {
        $this->socials()->save( $social );
    }

    public function addSocials( Collection $socials ) {
        $this->socials()->saveMany( $socials );
    }

}
