@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="list-group">
                        <a href="{{ route('artists.index') }}" class="list-group-item list-group-item-action">
                            Artistes
                        </a>
                        <a href="{{ route('organizations.index') }}" class="list-group-item list-group-item-action">
                            Organisations
                        </a>
                        <a href="{{ route('timetables.index') }}" class="list-group-item list-group-item-action">
                            Timetables
                        </a>
                        <a href="{{ route('posts.index') }}" class="list-group-item list-group-item-action active">
                            Articles
                        </a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
