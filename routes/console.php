<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('dummies', function () {

    //$styles
    $styles = factory(App\Style::class, 8)->create();
    $this->comment('Generating styles...');

    // Organization
    $orgs = factory(App\Organization::class, 5)->create();
    $this->comment('Generating organizations...');

    // Artist
    $artists = factory( App\Artist::class, 20 )->create();
    $this->comment('Generating artists...');

    // Rooms
    $rooms = factory(App\Room::class, 2)->create();
    $this->comment('Generating rooms...');

    // Links
    $artists->each( function( $artist, $key ) use ( $styles, $rooms ) {

        // styles
        $all_styles = collect([]);
        $styles->each( function($item, $key) use ($all_styles) {
            if( rand(0, 1) ) $all_styles->push( $item );
        });
        $artist->addStyles( $all_styles );
        $this->comment('Linking style for artist ' . $key . '...');

        // timetables
        factory(App\Timetable::class)->create([
            'artist_id' => $artist->id,
            'room_id' => $rooms->random()->id
        ]);
        $this->comment('Generating timetables for artist ' . $key . '...' );

    });

    // Add socials
    $artists->merge( $orgs )->each( function( $item, $key ){

        // socials
        $rsbase = [
            'facebook',
            'twitter',
            'mail',
            'application',
            'spottify',
            'soundcloud'
        ];
    
        $nb_social = rand(1, 4);
        for( $i=0; $i < $nb_social; $i++ ) {
            $key = rand(0, count($rsbase)-1);
            $rs = $rsbase[ $key ];
            $social = factory(App\Social::class)->create([
                'label' => $rs,
                'info_id' => $item->info->id
            ]);
            unset( $rsbase[$key] );
            $rsbase = array_values( $rsbase );
        }
        $this->comment('Generating socials for info ' . $item->info->id . '...');

        $nb_photo = rand(1, 4);
        for( $i=0; $i<$nb_photo; $i++) {
            factory(App\Photo::class)->create([
                'info_id' => $item->info->id
            ]);
        }
        $this->comment('Generating photos for info ' . $item->info->id . '...');

    } );

    // Posts
    factory(App\Post::class, 30)->create();
    $this->comment('Generating posts ...');


})->describe('Create dummy datas');
