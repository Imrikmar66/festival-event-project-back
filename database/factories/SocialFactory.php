<?php

use Faker\Generator as Faker;

$factory->define(App\Social::class, function (Faker $faker, $args) {
    $icon = App\SocialIcon::all()->random();
    return [
        'label' => $args['label'],
        'icon_id' => $icon->id,
        'url' => $faker->url, 
        'info_id' => $args['info_id']
    ];
});
