<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Info;
use App\Http\Requests\StoreArtistRequest;
use Illuminate\Support\Facades\Storage;

class ArtistController extends AuthenticatedController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artists.index')->withArtists( Artist::all() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArtistRequest $request)
    {

        $validated = collect($request->validated());

        $artist = new Artist;

        $info = Info::create( $validated->only('name', 'description')->all() );
        $organization = $request->getOrganization();
        
        $artist = Artist::create([
            'organization_id' => $organization->id,
            'info_id' => $info->id
        ]);

        $photos = $request->getPhotos();
        $styles = $request->getStyles();
        $socials = $request->getSocials();
        
        if( $photos->isNotEmpty() ){
            
            foreach( $photos as $photo ) {
                $path = Storage::putFile('artists', $photo->url);
                $photo->url = $path;
            }

            $artist->info->addPhotos( $photos );
        }

        if( $styles->isNotEmpty() )
            $artist->addStyles( $styles );

        if( $socials->isNotEmpty() )
            $artist->info->addSocials( $socials );

        
        return redirect( route('artists.show', [ $artist->id ]) );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist)
    {
        //
        return view('artists.show', compact('artist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        //
    }
}
