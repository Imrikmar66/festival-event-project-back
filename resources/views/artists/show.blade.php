@extends('layouts.app')

@section('content')

    <section id="artist">

        <h1> {{ $artist->info->name }} </h1>

        <img width="300px" src="{{ Storage::url( $artist->info->photos->first()->url ) }}" />

        <p> {{ $artist->info->description }} </p>
        
        <h2> Styles: </h2>
        @foreach (App\Style::all() as $style)
            <div class="form-check">
                <label for="styles" class="form-check-label "> 
                    <input 
                        type="checkbox"
                        name="styles[]" 
                        value="{{ $style->id }}" 
                        class="form-check-input"
                        @if( $artist->hasStyle( $style ) ) checked="true" @endif
                        >   
                    {{ $style->label }}
                </label>
            </div>
        @endforeach

        
    </section>

@endsection