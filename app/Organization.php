<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    public function info() {
        return $this->belongsTo( Info::class );
    }

    public function artist() {
        return $this->hasOne( Artist::class );
    }

    public function attachArtist( Artist $artist ) {
        $this->artist()->attach( $artist );
    }

}
