<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $user
        $user = factory(App\User::class)->create();
        $this->command->info('Generating user admin');

        // $styles
        $styles = factory(App\Style::class, 8)->create();
        $this->command->info('Generating styles...');

        // Organization
        $orgs = factory(App\Organization::class, 5)->create();
        $this->command->info('Generating organizations...');

        // Artist
        $artists = factory( App\Artist::class, 10 )->create();
        $this->command->info('Generating artists...');

        // Rooms
        $rooms = factory(App\Room::class, 2)->create();
        $this->command->info('Generating rooms...');

        // Links
        $artists->each( function( $artist, $key ) use ( $styles, $rooms ) {

            // styles
            $all_styles = collect([]);
            $styles->each( function($item, $key) use ($all_styles) {
                if( rand(0, 1) ) $all_styles->push( $item );
            });
            $artist->addStyles( $all_styles );
            $this->command->info('Linking style for artist ' . $key . '...');

            // timetables
            factory(App\Timetable::class)->create([
                'artist_id' => $artist->id,
                'room_id' => $rooms->random()->id
            ]);
            $this->command->info('Generating timetables for artist ' . $key . '...' );

        });

        // Add social icons
        $socialicons = factory( App\SocialIcon::class, 5 )->create();
        $this->command->info('Generating social icons...');

        // Add socials
        $artists->merge( $orgs )->each( function( $item, $key ){

            // socials
            $rsbase = [
                'facebook',
                'twitter',
                'mail',
                'application',
                'spottify',
                'soundcloud'
            ];
        
            $nb_social = rand(1, 4);
            for( $i=0; $i < $nb_social; $i++ ) {
                $key = rand(0, count($rsbase)-1);
                $rs = $rsbase[ $key ];
                $social = factory(App\Social::class)->create([
                    'label' => $rs,
                    'info_id' => $item->info->id
                ]);
                unset( $rsbase[$key] );
                $rsbase = array_values( $rsbase );
            }
            $this->command->info('Generating socials for info ' . $item->info->id . '...');

            $nb_photo = rand(1, 4);
            for( $i=0; $i<$nb_photo; $i++) {
                factory(App\Photo::class)->create([
                    'info_id' => $item->info->id
                ]);
            }
            $this->command->info('Generating photos for info ' . $item->info->id . '...');

        } );

        // Posts
        factory(App\Post::class, 30)->create();
        $this->command->info('Generating posts ...');
    }
}
