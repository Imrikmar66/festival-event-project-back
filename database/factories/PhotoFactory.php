<?php

use Faker\Generator as Faker;

$factory->define(App\Photo::class, function (Faker $faker, $args) {
    return [
        'url' => $faker->imageUrl(600, 400),
        'info_id' => $args['info_id']
    ];
});
