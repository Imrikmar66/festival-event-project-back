<?php

use Faker\Generator as Faker;

$factory->define(App\Artist::class, function (Faker $faker) {
    $info_id = factory(App\Info::class)->create()->id;
    $organization_id = App\Organization::all()->random()->id;
    return [
        'info_id' => $info_id,
        'organization_id' =>$organization_id
    ];
});
