<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Artist extends Model
{

    protected $fillable = ['organization_id', 'info_id'];

    public function styles() {
        return $this->belongsToMany( Style::class );
    }

    public function info() {
        return $this->belongsTo( Info::class );
    }

    public function organization() {
        return $this->belongsTo( Organization::class );
    }
    
    public function addStyle( Style $style ) {
        $this->styles()->save( $style );
    }

    public function hasStyle( Style $style ): bool {
        return $this->styles->contains( $style );
    }

    public function addStyles( Collection $styles ) {
        $this->styles()->saveMany( $styles->all() );
    }
}
