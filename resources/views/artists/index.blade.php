@extends('layouts.app')

@section('content')
    
    <section class="list artist-list">
        <div class="row p-3">
            
            @foreach ($artists as $artist)

            <div class="col-sm-4 mb-3"> @include('artists.bloclist') </div>

            @endforeach

        </div>
    </section>

    <section class="add add-artist">
        <h2> Add artist </h2>
        <form method="POST" action={{ route('artists.store') }} enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="name" class="form-check-label"> Artist name </label>
                <input type="text" name="name" placeholder="Artist name" class="form-control" >
            </div>
            <div class="form-group">
                <label for="description" class="form-check-label"> Artist description </label>
                <textarea name="description" placeholder="Artist description..." class="form-control" ></textarea>
            </div>
            <div>
                <label> Artist styles </label>
                <div class="b-container">
                    @foreach (App\Style::all() as $style)
                        <div class="form-check">
                            <label for="styles" class="form-check-label "> 
                                <input type="checkbox" name="styles[]" value="{{ $style->id }}" class="form-check-input" >   
                                {{ $style->label }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div>
                <label> Artist organization </label>
                <div class="b-container">
                    @foreach (App\Organization::all() as $organization)
                        <div class="form-check">
                            <label for="organization" class="form-check-label "> 
                                <input type="radio" name="organization" value="{{ $organization->id }}" class="form-check-input" >   
                                {{ $organization->info->name }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div id="form-pictures" class="form-group">
                <label for="pictures" class="form-check-label"> Pictures </label>
                <div class="pictures-container b-container">
                    <input type="file" name="pictures[]" class="form-control-file" >
                </div>
                <button id="add-picture" class="btn btn-success"> <i class="fas fa-plus"></i> Add picture </button>
            </div>
            <div id="form-socials"  class="form-group">
                <label class="form-check-label"> Socials </label>
                <div class="socials-container b-container">
                    <input type="text" name="social_labels[]" placeholder="label: Artist soundcloud..." class="form-control" >
                    <input type="text" name="social_urls[]" placeholder="url: http://..." class="form-control" >
                    @foreach (App\SocialIcon::all() as $icon)
                        <div class="form-check">
                            <label for="icon" class="form-check-label">
                                <input type="radio" name="icon_0" value="{{ $icon->id }}" class="form-check-input" >   
                                <img src="{{ asset('icons/'.$icon->url) }}">
                            </label>
                        </div>
                    @endforeach
                </div>
                <button id="add-social" class="btn btn-success"> <i class="fas fa-plus"></i> Add social </button>
            </div>

            <input type="submit" value="Add artist" class="btn btn-primary">
        </form>
    </section>

@endsection