<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Login */
Route::get('/home', 'HomeController@index')->name('home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

/* Resources */
Route::resource('posts',         'PostController');
Route::resource('artists',       'ArtistController');
Route::resource('organizations', 'OrganizationController');
Route::resource('photos',        'PhotoController');
Route::resource('rooms',         'RoomController');
Route::resource('socials',       'SocialController');
Route::resource('styles',        'StyleController');
Route::resource('timetables',    'TimetableController');